"use strict";
/* 
Теоретичні питання
1. Як можна визначити, яку саме клавішу клавіатури натиснув користувач?

window.addEventListener("keydown",(event) => {
console.log(event.code);
console.log(event.key);
})

2. Яка різниця між event.code() та event.key()?
Властивість key об’єкта події дозволяє отримати символ,і він може бути різним. Властивість code об’єкта події дозволяє отримати “фізичний код клавіші”, він завжди однаковий.

3. Які три події клавіатури існує та яка між ними відмінність?
- keydown;
- keyup;
- keypress (deprecated)
Різниця між keydown та keyup в тому що при натисканні keydown багато разів спрацьовує автоповторювання і коли клавішу відпускають спрацьовує keyup. 
keyup же можна зробити тільки один раз. 



Практичне завдання.
Реалізувати функцію підсвічування клавіш.

Технічні вимоги:

- У файлі index.html лежить розмітка для кнопок.
- Кожна кнопка містить назву клавіші на клавіатурі
- Після натискання вказаних клавіш - та кнопка, на якій написана ця літера, 
повинна фарбуватися в синій колір. При цьому якщо якась 
інша літера вже раніше була пофарбована в синій колір - 
вона стає чорною. Наприклад за натисканням Enter перша кнопка забарвлюється у 
синій колір. Далі, користувач натискає S, і кнопка S забарвлюється в синій колір, 
а кнопка Enter знову стає чорною.
*/

const buttonsCollection = document.querySelectorAll(".btn");
window.addEventListener("keydown", (event) => {

    buttonsCollection.forEach((el) => {
        el.style.backgroundColor = "black";
    });

    const buttonElement = document.querySelector(`.${event.code.toLowerCase()}-btn`);

    if (buttonElement) {
        buttonElement.style.backgroundColor = "blue";
    }
});
